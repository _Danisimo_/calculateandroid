package com.example.calculatorandroid1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.buttonToResult);
        button.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner spinner = findViewById(R.id.spinner);
                String operator = spinner.getSelectedItem().toString();

                int num1 = 0;
                int num2 = 0;

                EditText numOneEdit = findViewById(R.id.scrnNum1);
                EditText numTwoEdit = findViewById(R.id.scrnNum2);

                TextView showResult = findViewById(R.id.result);

                try {
                    num1 = Integer.parseInt(numOneEdit.getText().toString());
                    num2 = Integer.parseInt(numTwoEdit.getText().toString());
                }catch (Exception e){
                    showResult.setText("Введите число");
                    return;
                }

                System.out.println(num1);
                System.out.println(num2);

                double result;
                switch (operator) {
                    case "+":  result = num1+num2;
                        break;
                    case "-":  result = num1-num2;
                        break;
                    case "*":  result = num1*num2;
                        break;
                    case "/":
                        if(num2==0) {
                            Toast.makeText(getApplicationContext(), "На ноль не делим!", Toast.LENGTH_SHORT).show();
                            return;
                        }else
                            result = num1/num2;
                        break;
                    default: result = 0;
                        break;
                }

                showResult.setText(Integer.toString((int) result));
            }

        }));
    }
}



